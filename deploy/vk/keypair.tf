resource "tls_private_key" "this" {
  algorithm = "RSA"
}

resource "vkcs_compute_keypair" "key" {
  name = "${var.name}-key"
  public_key = tls_private_key.this.public_key_openssh
}

resource "local_file" "private_key_ssh" {
  content  = tls_private_key.this.private_key_pem
  filename = "${path.module}/ssh/key.pem"
  file_permission = "0400"
}