output "public_url" {
  description = "Public URL"
  value       = join("", concat(["http://"], [vkcs_networking_floatingip.compute.address]))
}
