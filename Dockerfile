FROM bellsoft/liberica-openjdk-alpine:11
COPY build/libs/vk-sample-11-11-*.jar app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
EXPOSE 8080
